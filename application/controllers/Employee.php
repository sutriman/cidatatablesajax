<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

  public function __construct(){

    parent::__construct();
    $this->load->helper('url');

    // Load model
    $this->load->model('M_employee');

  }

  public function index(){

    // load view
    $this->load->view('v_employee');

  }

  public function empList(){ #ini ajax
    // POST data
    $postData = $this->input->post();

    // Get data
    $data = $this->M_employee->getEmployees($postData);

    echo json_encode($data);
  }

}