-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: employe
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(60) NOT NULL,
  `salary` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `city` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,'Sutriman','4000','male','Yogyakarta','hi@sutriman.com'),(2,'Akmal','30000','male','Magelang','dummies@gmail.com'),(3,'Andri','35000','male','Yogyakarta','dummies@gmail.com'),(4,'Paijo','25000','male','Yogyakarta','dummies@gmail.com'),(5,'Sania','50000','female','Yogyakarta','dummies@gmail.com'),(6,'Ade','48000','male','Yogyakarta','dummies@gmail.com'),(7,'Aditya','36000','male','Yogyakarta','dummies@gmail.com'),(8,'Dimas','32000','male','Yogyakarta','dummies@gmail.com'),(9,'Jul','48000','male','Yogyakarta','dummies@gmail.com'),(10,'Ahmad','52000','male','Yogyakarta','dummies@gmail.com'),(11,'Madi','48000','male','Yogyakarta','dummies@gmail.com'),(12,'Supri','54000','male','Yogyakarta','dummies@gmail.com'),(13,'Sujad','43000','female','Yogyakarta','dummies@gmail.com'),(14,'Rafi','32000','male','Yogyakarta','dummies@gmail.com'),(15,'Surati','45000','female','Yogyakarta','dummies@gmail.com'),(16,'Sunaryo','38000','male','Yogyakarta','dummies@gmail.com'),(17,'Kambing','47000','male','Yogyakarta','dummies@gmail.com'),(18,'Sapi','28000','female','Yogyakarta','dummies@gmail.com'),(19,'Coro','34000','male','Yogyakarta','dummies@gmail.com'),(20,'Kecoa','34000','male','Yogyakarta','dummies@gmail.com'),(21,'Kucing','41000','female','Yogyakarta','dummies@gmail.com'),(22,'Apel','28000','male','Yogyakarta','dummies@gmail.com'),(23,'Anggur','46000','male','Yogyakarta','dummies@gmail.com'),(24,'Arab','28000','male','Yogyakarta','dummies@gmail.com'),(25,'Lebah','32000','female','Yogyakarta','dummies@gmail.com'),(26,'Semut','44000','male','Yogyakarta','dummies@gmail.com'),(27,'Anton','30000','female','Yogyakarta','dummies@gmail.com'),(28,'Rujak','28000','male','Yogyakarta','dummies@gmail.com'),(29,'Cingur','32000','female','Yogyakarta','dummies@gmail.com'),(30,'Suparni','32000','female','Yogyakarta','dummies@gmail.com'),(31,'Mayur','28000','male','Yogyakarta','dummies@gmail.com'),(32,'Sayur','32000','male','Yogyakarta','dummies@gmail.com'),(33,'Lodeh','33000','male','Yogyakarta','dummies@gmail.com');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-29 16:37:47
